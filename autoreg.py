import os
import time
import csv

from random import randint

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ChromeOptions
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

import exrex

from datetime import datetime

URL = 'https://account.mail.ru/signup?from=main&rf=auth.mail.ru'

DRIVER_PATH = os.path.abspath("./driver/chrome/chromedriver_mac")

MONTH_SELECT = ['Январь',
                'Февраль',
                'Март',
                'Апрель',
                'Май',
                'Июнь',
                'Июль',
                'Август',
                'Сентябрь',
                'Октябрь',
                'Ноябрь',
                'Декабрь'
                ]

SEX_SELECT = ['male',
              'female'
              ]

DOMAIN_SELECT = ['@mail.ru',
                 '@inbox.ru',
                 '@list.ru',
                 '@bk.ru'
                 ]


class Browser:

    def __init__(self):
        self.options = ChromeOptions()
        # self.options.add_argument("--headless")
        # self.options.add_argument("--start-maximized")
        self.driver = webdriver.Chrome(DRIVER_PATH, options=self.options)
        self.driver.maximize_window()
        self.wait = WebDriverWait(self.driver, 100)
        self.driver.get(URL)

    def close(self):
        self.driver.close()





def generate_data():
    firstname = exrex.getone(r'[a-z][A-Z[1-9]{1,20}')
    lastname = exrex.getone(r'[a-z][A-Z[1-9]{1,20}')
    email_login = exrex.getone(r'[a-zA-Z0-9\_\.\-]{4,31}')
    password = exrex.getone(r'[a-zA-Z0-9]{10,12}')
    return firstname, lastname, email_login, password


def fields_filling(name, lastname, email_login, generated_password):
    selected_domain = DOMAIN_SELECT[randint(0, 3)]
    first_name = browser.wait.until(
        EC.presence_of_element_located((By.NAME, "firstname")))
    actions.move_to_element(first_name).click().send_keys(name)
    last_name = browser.wait.until(
        EC.presence_of_element_located((By.NAME, "lastname")))
    actions.move_to_element(last_name).click().send_keys(lastname)
    day_of_birth = browser.wait.until(
        EC.presence_of_element_located((By.CLASS_NAME, "b-date__day"))).click()
    day_pick = browser.wait.until(EC.presence_of_element_located((
        By.XPATH, "//*[@class=\'b-dropdown__list__item day{}\']".format(randint(1, 28))))).click()
    month_of_birth = browser.wait.until(
        EC.presence_of_element_located((By.CLASS_NAME, "b-date__month"))).click()
    month_pick = browser.wait.until(EC.presence_of_element_located((
        By.XPATH, "//*[@data-text=\'{}\']".format(MONTH_SELECT[randint(0, 11)])))).click()
    year_of_birth = browser.wait.until(
        EC.presence_of_element_located((By.CLASS_NAME, "b-date__year"))).click()
    year_pick = browser.wait.until(EC.presence_of_element_located((
        By.XPATH, "//*[@data-text=\'{}\']".format(randint(1901, 2018))))).click()
    sex = browser.wait.until(
        EC.presence_of_element_located((By.CLASS_NAME, "b-radiogroup__input__label"))).click()
    sex_pick = browser.wait.until(EC.presence_of_element_located((
        By.XPATH, "//*[@data-mnemo=\'sex-{}\']".format(SEX_SELECT[randint(0, 1)])))).click()
    email = browser.wait.until(
        EC.presence_of_element_located((By.CLASS_NAME, "b-email__name")))
    actions.move_to_element(email).click().send_keys(email_login)
    password = browser.wait.until(
        EC.presence_of_element_located((By.NAME, "password")))
    actions.move_to_element(password).click().send_keys(generated_password)
    domain = browser.wait.until(
        EC.presence_of_element_located((By.CLASS_NAME, "b-email__domain"))).click()
    domain_pick = browser.wait.until(EC.presence_of_element_located((
        By.XPATH, "//*[@data-text=\'{}\']".format(selected_domain)))).click()
    password_repeat = browser.wait.until(
        EC.presence_of_element_located((By.NAME, "password_retry")))
    actions.move_to_element(
        password_repeat).click().send_keys(generated_password).perform()
    button_submit = browser.wait.until(
        EC.visibility_of_element_located((By.CLASS_NAME, "btn__text"))).click()
    return selected_domain
    

def save_data(email, password, selected_domain):
    while(1):
        if browser.driver.current_url == 'https://e.mail.ru/inbox/?newreg=1&signup_b=1&sms_reg=1&features=1&afterReload=1':
            with open('registered_emails_{}.csv'.format(datetime.today().time()), 'a') as csvfile:
                result_writer = csv.writer(csvfile)
                result_writer.writerow([email+selected_domain, password])
                break


i = 0
while(i != 10):
    browser = Browser()
    actions = ActionChains(browser.driver)
    name, lastname, email, password = generate_data()
    selected_domain = fields_filling(name, lastname, email, password)
    save_data(email, password, selected_domain)
    browser.close()
    i += 1

# generate random data based on regEXP
